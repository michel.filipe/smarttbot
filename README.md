# Teste Devops - Smarttbot

As alterações no repositório foram:

- [docker-compose.yml](docker-compose.yml) para rodar a aplicação em desenvolvimento;
- `Dockerfile` do [frontend](frontend/Dockerfile) e [backend](backend/Dockerfile) para criarem as imagens das aplicações;
- [.gitlab-ci.yml](.gitlab-ci.yml) para as instruções de CI/CD usando Gitlab CI. As [imagens se encontram aqui](https://hub.docker.com/repository/docker/mfilipe/smarttbot).

## How-to

Rodar toda a aplicação localmente:
```shell
$ docker-compose up --build
```
Sobe toda a aplicação através do `docker-compose`, dando build nas imagens e disponibilizando em http://localhost:3000.

Rodar o frontend:
```shell
$ docker build -t chatapp:frontend frontend
$ docker run -p 3000:3000 chatapp:frontend
```
Dá build na imagem do frontend e disponibiliza em http://localhost:3000.

Rodar o backend:
```shell
$ docker network create chatapp
$ docker run -d --net chatapp --name redis redis
$ docker build --build-arg REDIS_ADDR=redis.chatapp:6379 -t chatapp:backend backend
$ docker run --net chatapp -p8080:8080 chatapp:backend
```
Cria a rede chatapp para comunicação entre o redis e backend, dá build na imagem do backend e disponibiliza em http://localhost:8080.